#!/bin/bash

find . -type f -name "*.lnk" -exec rm {} \;
find . -type f -name "*.Ink" -exec rm {} \;
find . -type f -name "*.inf" -exec rm {} \;
find . -type f -name "*.INF" -exec rm {} \;
find . -type f -name "*.vbs" -exec rm {} \;
find . -type f -name "*.vbe" -exec rm {} \;
find . -type f -name "*.exe" -exec rm {} \;
find . -type f -name "*.db" -exec rm {} \;
find . -type f -name "*.dat" -exec rm {} \;
find . -type f -name "*.tmp" -exec rm {} \;
find . -type f -name "*.ini" -exec rm {} \;
find . -type f -name "*.INI" -exec rm {} \;

rm -R RECYCLER
rm -R RECYCLERING
rm -R FOUND.000
rm -R .Trash-1000
rm -R .Trash-usuario
